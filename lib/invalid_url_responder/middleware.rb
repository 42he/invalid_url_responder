module InvalidUrlResponder
  class Middleware
    def initialize app
      @app = app
    end

    def call env
      begin
        @app.call(env)

      rescue URI::InvalidURIError
        ['400', {'Content-Type' => 'text/plain'}, ['Not Supported URL encoding']]
      rescue ActionController::UnknownHttpMethod
      	['405', {'Content-Type' => 'text/plain'}, ['Not Supported http method']]
      end

    end
  end
end
