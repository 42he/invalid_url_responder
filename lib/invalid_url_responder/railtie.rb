module InvalidUrlResponder
  class Railtie < Rails::Railtie
    initializer "invalid_url_responder.configure_rails_initialization" do |app|
      app.middleware.use InvalidUrlResponder::Middleware
    end
  end
end
