# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'invalid_url_responder/version'

Gem::Specification.new do |spec|
  spec.name          = "invalid_url_responder"
  spec.version       = InvalidUrlResponder::VERSION
  spec.authors       = ["Arne Buchner"]
  spec.email         = ["arne.buchner@42he.com"]

  spec.summary       = "Fixes Bad Uri requests"
  spec.description   = "Fixes Bad Uri requests"
  spec.homepage      = "http://42he.com"
  spec.license       = "MIT"


  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest"
end
